The food must taste good, giving the pleasure to eat it and must be produced respecting the environment, animal welfare and health The Italian way of eating exalts the genius of a people who pride themselves on artistic self expression in drawing from the assets of a land that is intricately diverse by nature. Human and natural elements have combined in kitchens over time to engender an astonishing verity of dishes.
That explains why IL FORNO does not represent a national wat of cooking so much as an encyclopedic fund of recipes, techniques and flavors of local inspiration.
Italian food has triumphed at all levels around the world, in temples of gourmet dining as well as in cafes, pizza parlors, wine bars, fancy food shops, supermarkets and corner groceries. Innovation and respect for the original taste and recipe are the only wat to produce authentic.
The Company of IL FORNO was born 18 years ago and since that time we are following the same philosophy we brought to Abu Dhabi the highest standard of Italian cuisine.
Website: https://ilforno.me/
Email: media@ilfornogroup.com
Address: Dubai, Abu Dhabi, UAE
